import OFXUser from './user';

export class PathHelper {

  static kycCustomerFilesPath(user: OFXUser, title: string) {
    return `userData/${user.id}/kycCustomer/${title}`;
  }

  static kycTraderFilesPath(user: OFXUser, title: string) {
    return `userData/${user.id}/kycTrader/${title}`;
  }
}


export class OFXFileType {
  type: string;
  extention: string;
}


export class OFXImage {
  path: string;
  downloadURL: string;
  urerID: string;
}

export class OFXFile {

  constructor(dirPath, id) {
    this.dirPath = dirPath
    this.id = id
    this.path = dirPath + '/' + id
  }
  id: string
  title: string;
  description: string;
  downloadURL?: string;
  acceptedFileTypes: string[];
  type?: OFXFileType = new OFXFileType;
  dirPath: string
  path?: any; // a function to return
}
