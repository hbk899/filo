import { OFXFile } from './storage';


export class Upload {

  $key: string;
  ofxFile: OFXFile;
  file: any;
  name: string;
  url: string;
  progress: number;
  createdAt: Date = new Date();
  isUploading: boolean;

  loading: boolean;
  constructor(ofxFile: OFXFile) {
    this.ofxFile = ofxFile;
  }
}
