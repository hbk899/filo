import { Image } from './image'
import { OFXFile } from './storage';

export class Post {

  static ALL_CATEGORIES = {
    'cow': {
      'male-cow': 'male-cow'
    },
    'goat': {
      'male-goat': 'male-goat'
    }
  }

  constructor(id) {
    this.id = id;
  }

  id: string
  title: string
  description: string

  images: OFXFile[]
  price: number
  phoneNo: string
  userID: string


  keywords: string[]
  category: string

}


