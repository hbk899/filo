import { Tags } from "./tags"


export class Article {
  id: string
  url: string
  title: string
  los: string
  body: string = ''
  description: string
  parsedBody?: string
  createdAt: number
  updatedAt: number
  topics: Tags
}


export class ArticleMini {
  id: string
  url: string
  title: string
  createdAt: number
  updatedAt: number
  los: string
  description: string
  topics: Tags


  public static fromArticle(article: Article) {
    return <ArticleMini>{
      id: article.id,
      url: article.url,
      title: article.title,
      los: article.los,
      createdAt: article.createdAt,
      updatedAt: article.updatedAt,
      description: article.description,
      topics: article.topics
    }
  }
}
