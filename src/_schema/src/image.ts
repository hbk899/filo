export class Image {

  id: string
  path: string
  downloadURL: string
}
