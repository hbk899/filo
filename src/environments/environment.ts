// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBijkptqPF14AwGwD0yfNkRHJJ_S1s9nsk',
    authDomain: 'bilu-mandi.firebaseapp.com',
    databaseURL: 'https://bilu-mandi.firebaseio.com',
    projectId: 'bilu-mandi',
    storageBucket: 'bilu-mandi.appspot.com',
    messagingSenderId: '33520871041',
    appId: '1:33520871041:web:51a24c335ee98132dacf80',
    measurementId: 'G-2VNDQ1E7E8'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
