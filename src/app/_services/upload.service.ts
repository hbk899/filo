import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';

import IUser from 'src/_schema/src/user';


import { storage } from 'firebase';

import { AuthService } from './auth.service';
import { NotifyService } from './notify.service';
import { Upload } from 'src/_schema/src/upload';
import { OFXFile } from 'src/_schema/src/storage';


@Injectable({ providedIn: 'root' })
export class UploadService {



  currentUser: IUser;

  constructor(
    private db: AngularFirestore,
    private afStorage: AngularFireStorage,
    private authService: AuthService,
    private notifyService: NotifyService) {



  }




  upload(upload: Upload) {

    const ref = this.afStorage.ref(upload.ofxFile.path);
    return ref.put(upload.file)
      .then(snapshot => {
        return snapshot.ref.getDownloadURL();
      })
      .then(downloadUrl => {
        return downloadUrl;
      });
  }


  async deleteFile(file: OFXFile) {
    return this.afStorage.ref(file.path).delete()
  }

  uploadAndGetRef(upload: Upload) {

    const ref = this.afStorage.ref(upload.ofxFile.path);
    return ref.put(upload.file)
      .then(snapshot => {
        return snapshot.ref;
      })
  }



  async listFiles(path: string) {
    return (await storage().ref(path).list()).items
  }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
