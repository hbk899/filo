import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import * as firebase from 'firebase';

import IUser from 'src/_schema/src/user';
import { Post } from 'src/_schema/src/post';

import { allRandomNames } from '../_utils/randoms';

import { AuthService } from './auth.service';
import { NotifyService } from './notify.service';




/**
 * Just using it for simple database changes
 */
@Injectable({ providedIn: 'root' })
export class PostService {




  constructor(
    private db: AngularFirestore,
    private authService: AuthService,
    private notifyService: NotifyService,
    private http: HttpClient) {

  }



  async savePost(post: Post) {
    post.userID = this.authService.mUser.id;
    return this.db.collection('posts').doc(post.id).set(JSON.parse(JSON.stringify(post)), { merge: true })
  }
  async getPost(postId: string): Promise<Post> {
    return this.db.collection('posts').doc(postId).ref
      .get()
      .then(doc => {
        return doc.data() as Post
      })
  }




  async allPosts(): Promise<Post[]> {
    return this.db.collection('posts').ref.limit(30).get()
      .then(snapshot => {
        return snapshot.docs.map(doc => {
          return doc.data() as Post
        })
      })
  }




  async myPosts() {
    return this.db.collection('posts').ref.limit(30).get()
      .then(snapshot => {
        return snapshot.docs.map(doc => {
          return doc.data() as Post
        })
      })
  }

}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
