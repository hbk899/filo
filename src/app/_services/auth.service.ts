import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';

import * as firebase from 'firebase';

import { Observable, of, merge, BehaviorSubject } from 'rxjs';
import { switchMap, startWith, map, tap, filter, publish, publishLast, refCount, mergeAll, share, shareReplay } from 'rxjs/operators';

import { User } from 'firebase';

import IUser from 'src/_schema/src/user';

import { NotifyService } from './notify.service';
import { WindowService } from './window.service';



// interface User {
//   uid: string;
//   email?: string | null;
//   photoURL?: string;
//   displayName?: string;
// }

@Injectable()
export class AuthService {



  mUser: IUser;
  mUser$ = new BehaviorSubject<IUser>(null);



  constructor(
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private afStorage: AngularFireStorage,
    private win: WindowService,
    private router: Router,
    private notify: NotifyService) {


    this.startUserUpdates();
  }


  private startUserUpdates(): void {

    this.afAuth.user.subscribe(user => {
      if (user) {
        console.log('oh maaaaaaaan');
        this.afs.collection('users').doc(user.uid)
          .valueChanges()
          .pipe(
            tap(user => {
              console.log('user', user);
              this.mUser = user as IUser;
              localStorage.setItem('currentUser', JSON.stringify(user))
            })
          )
          .subscribe(this.mUser$)
      } else {
        console.log('oh maaaaaaaan null');
        return of(null);
      }
    })

  }



  googleLogin(type: 'redirect' | 'popup'): Promise<User | any> {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider, type);
  }

  facebookLogin(type: 'redirect' | 'popup'): Promise<User | any> {
    const provider = new firebase.auth.FacebookAuthProvider();
    return this.oAuthLogin(provider, type);
  }

  private oAuthLogin(provider: any, type: 'redirect' | 'popup') {
    if (type === 'redirect') {
      return this.afAuth.auth
        .signInWithRedirect(provider)
        .then(credential => {
          this.notify.update('Welcome', 'success');
          return credential;
        })
        .catch(e => {
          this.notify.update(e.message, 'error');
        });
    }
    else {
      return this.afAuth.auth
        .signInWithPopup(provider)
        .then(credential => {
          this.notify.update('Welcome', 'success');
          return credential;
        })
        .catch(e => {
          this.notify.update(e.message, 'error');
        });
    }
  }

  //// Anonymous Auth ////
  anonymousLogin() {
    return this.afAuth.auth
      .signInAnonymously()
      .then(credential => {

        this.notify.update('Welcome', 'success');
        return credential;
      });
  }

  //// Email/Password Auth ////
  emailSignUp(newUser: IUser, password: string) {

    return this.afAuth.auth
      .createUserWithEmailAndPassword(newUser.email, password)
      .then(credential => {
        console.log(credential);
        newUser.id = credential.user.uid;

        // this.startUserUpdates();

        return this.createUser(newUser);
      })
      .catch(e => {
        this.notify.update(e.message, 'error');
      });
  }

  emailLogin(email: string, password: string) {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(credential => {
        return this.afs.doc(`users/${credential.user.uid}`).ref.get();
      })
      .then(credentials => {

        // localStorage.setItem('currentUser', JSON.stringify(user.data));
        return credentials.data();
      })
      .then(user => {
        // this.startUserUpdates();
        this.notify.update('Welcome to Alpha!!!', 'success');
        return user;
      })
      .catch(e => {
        this.notify.update(e.message, 'error');
      });
  }




  logOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/signin']);
    });
  }

  async sendVerifyEmail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
      .then(ok => {
        this.notify.update(`Email sent to: ${this.afAuth.auth.currentUser.email}`, 'success')
      })
      .catch(e => {

        this.notify.update(`Error`, 'error')
      })
  }


  sendResetPasswordEmail(email) {
    return this.afAuth.auth.sendPasswordResetEmail(email)
      .then(() => {
        this.notify.update('Password reset email sent to : ' + email, 'success')
      })
      .catch((error) => {
        console.error(error);


        this.notify.update('Counld not send password reset email to : ' + email, 'error')
      })
  }


  getCurrentUser(): IUser {
    return JSON.parse(localStorage.getItem('currentUser'));
  }



  // user related...





  // If error, console log and notify user
  private handleError(error: Error) {
    console.error(error);
    this.notify.update(error.message, 'error');
  }


  public getLoggedInUpdates(): Observable<User> {
    return this.afAuth.user;
  }

  private updateUserData(userData: any, id: string): Promise<any> {

    console.log(userData);
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${id}`
    );
    let temp: any = JSON.stringify(userData);
    temp = JSON.parse(temp);


    if (!temp.createdAt) {
      temp.createdAt = firebase.firestore.FieldValue.serverTimestamp();
    } else {
      temp.updatedAt = firebase.firestore.FieldValue.serverTimestamp();
    }
    return userRef.update(temp);
  }

  public async updateUserSettings(settings) {
    return this.updateUserData({
      'settings': settings
    }, this.mUser.id)
  }


  private createUser(user: IUser): Promise<any> {
    console.log(user);
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${user.id}`
    );
    let temp: any = JSON.stringify(user);
    temp = JSON.parse(temp);
    if (!temp.createdAt) {
      temp.createdAt = firebase.firestore.FieldValue.serverTimestamp();
    } else {
      temp.updatedAt = firebase.firestore.FieldValue.serverTimestamp();
    }

    userRef.set(temp);
    return temp;

  }



  async syncUserToDb(user: User): Promise<any> {

    const toUpdate = {};
    toUpdate['id'] = user.uid;
    toUpdate['displayName'] = user.displayName;
    toUpdate['email'] = user.email;
    toUpdate['photoURL'] = user.photoURL;
    toUpdate['updatedAt'] = firebase.firestore.FieldValue.serverTimestamp();

    await this.afs.collection('users').doc(user.uid).set(toUpdate, {
      merge: true
    })


    return user;

  }



}
