import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFirestore, Query } from '@angular/fire/firestore';

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { firestore } from 'firebase';


import IUser from 'src/_schema/src/user';

import { Article, ArticleMini } from 'src/_schema/src/article';

import { AuthService } from './auth.service';
import { DirectService } from './direct.service';
import { NotifyService } from './notify.service';
import { UploadService } from './upload.service';






@Injectable({ providedIn: 'root' })
export class AdminService {




  constructor(
    private authService: AuthService,
    private db: AngularFirestore,
    private storageService: UploadService,
    private notifyService: NotifyService,
    private directService: DirectService) {


  }










  getNewID() {
    const r = this.db.createId();
    console.log('id-requested', r);

    return r
  }



  async newArticle(article: Article): Promise<Article> {

    const user = this.authService.getCurrentUser();
    // article.id = this.db.createId(); // id is already generated for new in component
    article.createdAt = (new Date).getTime()


    return this.updateArticle(article);

  }

  async updateArticle(article: Article): Promise<Article> {

    console.log('article to update', article);

    const user = this.authService.getCurrentUser();

    article.updatedAt = (new Date).getTime()

    article.createdAt = (new Date).getTime()

    const batch = firestore().batch();
    batch.set(firestore().collection('articles').doc(article.id), JSON.parse(JSON.stringify(article)), {
      merge: true
    })
    batch.set(firestore().collection('manual').doc('article-list'), {
      [article.id]: ArticleMini.fromArticle(article)
    },
      {
        merge: true
      })
    await batch.commit()



    return article;
  }


  async getFilesForArticle(id) {
    return this.storageService.listFiles(`article-images/${id}`);
  }



}


