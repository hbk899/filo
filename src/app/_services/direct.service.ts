import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { catchError, map, tap } from 'rxjs/operators';




import { AngularFirestore } from '@angular/fire/firestore';
import { NotifyService } from './notify.service';


import { allRandomNames } from '../_utils/randoms';
import * as firebase from 'firebase';
import IUser from 'src/_schema/src/user';




/**
 * Just using it for simple database changes
 */
@Injectable({ providedIn: 'root' })
export class DirectService {



    currentUser: IUser;

    constructor(
        private db: AngularFirestore,
        private notifyService: NotifyService,
        private http: HttpClient) {

    }


    neww(collection: string, id: string, object: any) {
        return new Promise<any>((resolve, reject) => {
            this.db.collection(collection).doc(id).set(object)

                .then((snapshot) => {
                    resolve(true);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
    update(collection: string, id: string, object: any) {
        return new Promise<any>((resolve, reject) => {
            this.db.collection(collection).doc(id).update(object)

                .then((snapshot) => {
                    resolve(true);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }


    getByIdAndCollection(collection: string, id: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.db.collection(collection).doc(id).ref.get()

                .then((snapshot) => {
                    if (snapshot.exists) {
                        resolve(snapshot.data());
                    } else {
                        reject('devvvvvvvvvvvvvvvvvcould not find');
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });

    }


    getCollection(collection: string): Promise<any[]> {

        return new Promise<any>((resolve, reject) => {
            this.db.collection(collection).ref.get()

                .then((snapshot) => {
                    if (!snapshot.empty) {
                        const result: any[] = [];
                        snapshot.docs.forEach(doc => {
                            result.push(doc.data());
                        });

                        resolve(result);
                    } else {
                        reject('could not find');
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });

    }




    getDocument(path: string): Promise<any[]> {

        return new Promise<any>((resolve, reject) => {
            this.db.doc(path).ref.get()

                .then((snapshot) => {
                    if (snapshot.exists) {
                        resolve(snapshot.data());
                    } else {
                        reject('could not find');
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });

    }


    get(endpoint): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.get(endpoint)
                .subscribe(response => {
                    resolve(response);

                });
        });

    }


    gett(collection: string, q?: any): Promise<any[] | void> {

        return new Promise<any[] | void>((resolve, reject) => {
            let options;
            let query;
            if (q) {
                options = Object.keys(q);
                query = this.db.collection(collection)
                    .ref.where(q[options[0]].key,
                        q[options[0]].operand,
                        q[options[0]].value);
                for (let i = 1; i < options.length; i++) {
                    query = query.where(q[options[i]].key,
                        q[options[i]].operand,
                        q[options[i]].value);
                }
            } else {
                query = this.db.collection(collection)
                    .ref;
            }


            query.get()
                .then((snapshot) => {
                    console.log(snapshot);

                    if (!snapshot.empty) {
                        const allDOCS: any[] = [];
                        snapshot.docs.forEach(d => {
                            allDOCS.push(d.data());
                        });
                        resolve(allDOCS);
                    } else {
                        resolve(null);
                    }
                })
                .catch(err => {
                    resolve(err);
                });
        });

    }


    updateField(collection: string, id: string, doc: any, fieldToUpdate: string): Promise<any> {

        return new Promise<any>((resolve, reject) => {

            const dataToUpdate = {};
            dataToUpdate[fieldToUpdate] = doc[fieldToUpdate];
            this.db.collection(collection).doc(id).update(JSON.parse(JSON.stringify(dataToUpdate)))
                .then(ok => {
                    resolve(null);
                })
                .catch(err => {
                    resolve(err);
                });
        });
    }

















    // Admin

    varifyKycCustomer(user: IUser) {

        const addMessage = firebase.functions().httpsCallable('approveCustomer');
        return addMessage({ user: user });
    }



}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
