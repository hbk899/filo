import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { MatChipInputEvent, MatAutocompleteSelectedEvent, MatTableDataSource } from '@angular/material';
import { ENTER, COMMA } from '@angular/cdk/keycodes';

import { Observable, Subject, of } from 'rxjs';

import OFXUser from 'src/_schema/src/user';

import { UploadService } from 'src/app/_services/upload.service';

import { AuthService } from '../../_services/auth.service';
import { Upload } from 'src/_schema/src/upload';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})



export class UploadComponent implements OnInit {

  user = new OFXUser;
  @Output() result = new EventEmitter<any>();


  error = false;


  filesToUpload: any;



  displayedColumns: string[] = ['file', 'description', 'actions'];
  dataSource = [];


  uploads = {};






  constructor(private authService: AuthService,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private location: Location) {

  }

  upload = (event: any, fileUpload: Upload) => {
    if (event.target.files !== undefined && event.target.files.length > 0) {
      // this.dpUpload = new Upload(event.target.files[0]);
      fileUpload.file = event.target.files[0];

      fileUpload.loading = true;
      this.uploadService.upload(fileUpload)
        .then(
          downloadUrl => {
            fileUpload.loading = false;
            fileUpload.ofxFile.downloadURL = downloadUrl;
            this.filesToUpload[fileUpload.ofxFile.title] = fileUpload.ofxFile;
            this.result.emit(this.filesToUpload);
            console.log(fileUpload);
            this.refreshTable();

          }
        )
        .catch(err => {
          console.log(err);

        });
    } else {
      // no files...
    }

  }


  ngOnInit(): void {




  }

  refreshTable() {

    this.dataSource = [];
    Object.keys(this.filesToUpload).forEach(title => {
      const upload = new Upload(this.filesToUpload[title]);
      this.dataSource.push(upload);
      this.uploads[title] = upload;
      console.log(upload);

    });
  }


  @Input('filesToUpload')
  set setFilesToUpload(filesToUpload: any) {
    this.filesToUpload = filesToUpload;

    console.log(this.filesToUpload);

    Object.keys(filesToUpload).forEach(title => {
      if (!this.uploads[title]) {
        const upload = new Upload(filesToUpload[title]);
        this.dataSource.push(upload);
        this.uploads[title] = upload;


        console.log(title);

      }
    });


    this.refreshTable();




  }



  @Input('user')
  set setUser(user: OFXUser) {
    this.user = user;
  }


}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
