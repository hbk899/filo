import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import IUser from 'src/_schema/src/user';
import { AuthService } from 'src/app/_services/auth.service';
import { Post } from 'src/_schema/src/post';

import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { DialogData } from '../alert-dialog/dialog-data';






@Component({
  selector: 'app-card-post',
  templateUrl: './card-post.component.html',
  styleUrls: ['./card-post.component.scss']
})
export class CardPostComponent implements OnInit {


  @Input() post: Post


  constructor(private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    public dialog: MatDialog) { }

  ngOnInit() {

  }






}

