import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import IUser from 'src/_schema/src/user';
import { AuthService } from 'src/app/_services/auth.service';
import { DialogData } from '../alert-dialog/dialog-data';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';






@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {


  loggedIn = false;
  notloggedIn = true;
  userId: any;
  username: any = '';
  authToken: any;



  routerPath = ''

  user = new IUser;

  breakpoint: any;



  constructor(private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.authService.getLoggedInUpdates().subscribe((authUser) => {
      if (authUser != null) {
        this.loggedIn = true;
        this.notloggedIn = false;
      }
    });

    this.authService.mUser$.subscribe((user) => {
      if (user != null) {
        this.user = user;

      }
    });

    this.breakpoint = (window.innerWidth <= 400) ? 1 : 6;

    this.router.events.subscribe((evt) => {


      // this.routerPath = this.route.snapshot;

    });

  }

  onResize(event) {
    console.log(window.innerWidth);

    this.breakpoint = (event.target.innerWidth <= 400) ? 1 : 6;
  }

  logout(): void {
    const options: any = {};
    options.width = '350px';
    options.data = <DialogData>{};
    options.data.message = 'Logout?';
    options.data.positive = 'Yes';
    options.data.negative = 'No';

    const dialogRef = this.dialog.open(AlertDialogComponent, options);

    dialogRef.afterClosed().subscribe(result => {
      if (result === options.data.positive) {
        this.authService.logOut();

        localStorage.clear();
        this.userId = 0;
        this.loggedIn = false;
        this.notloggedIn = true;
        this.username = '';
      }

    });
  }



}
