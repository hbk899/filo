export interface DialogData {
    message: string;
    positive: string;
    negative: string;

    qrData: any;
    result;
}
