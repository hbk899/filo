import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import IUser from 'src/_schema/src/user';
import { AuthService } from 'src/app/_services/auth.service';

import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { DialogData } from '../alert-dialog/dialog-data';






@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {


  loggedIn = false;
  notloggedIn = true;
  userId: any;
  username: any = '';
  authToken: any;



  routerPath = ''

  user = new IUser;


  breakpoint: any;
  url: string;



  constructor(private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.authService.getLoggedInUpdates().subscribe((authUser) => {
      if (authUser != null) {
        this.loggedIn = true;
        this.notloggedIn = false;
      }
    });

    this.authService.mUser$.subscribe((user) => {
      if (user != null) {
        this.user = user;

      }
    });

    this.breakpoint = (window.innerWidth <= 400) ? 1 : 6;



    this.url = this.router.url
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this.url = evt.url
      }
    });


  }

  onResize(event) {
    console.log(window.innerWidth);

    this.breakpoint = (event.target.innerWidth <= 400) ? 1 : 6;
  }

  logout(): void {
    const options: any = {};
    options.width = '350px';
    options.data = <DialogData>{};
    options.data.message = 'Logout?';
    options.data.positive = 'Yes';
    options.data.negative = 'No';

    const dialogRef = this.dialog.open(AlertDialogComponent, options);

    dialogRef.afterClosed().subscribe(result => {
      if (result === options.data.positive) {
        this.authService.logOut();

        localStorage.clear();
        this.userId = 0;
        this.loggedIn = false;
        this.notloggedIn = true;
        this.username = '';
      }

    });
  }



}

