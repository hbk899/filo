import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'keys' })
export class KeysPipe implements PipeTransform {
  transform(value, args: string[]): any {
    return Object.keys(value ? value : {});
  }
}




@Pipe({ name: 'keys_length' })
export class KeysLengthPipe implements PipeTransform {
  transform(value, args: string[]): any {
    return Object.keys(value ? value : {}).length;
  }
}



@Pipe({ name: 'deep_copy' })
export class DeepCopyPipe implements PipeTransform {
  transform(value, args: string[]): any {
    return JSON.parse(JSON.stringify(value))
  }

}


@Pipe({ name: 'trim' })
export class TrimPipe implements PipeTransform {
  transform(value: string, chars?: number): string {
    return value.substr(0, chars);
  }
}


@Pipe({ name: 'string_to_date' })
export class StringToDate implements PipeTransform {
  transform(value, args: string[]): any {
    return new Date(value)
  }

}


@Pipe({ name: 'time_seconds' })
export class SecondsToTimePipe implements PipeTransform {
  transform(seconds: number, unit?: 'all-words' | 'all' | 'min' | 'sec' | 'hr'): string {
    if (!seconds) {
      seconds = 0
    }

    if (unit == 'all') {

      const mins = Math.floor(seconds / 60);
      const secs = Math.floor(seconds) - (60 * Math.floor(seconds / 60))

      return `${(mins < 10 ? '0' : '') + mins} : ${(secs < 10 ? '0' : '') + secs}`
    }


    if (unit) {
      if (unit == 'sec') {

        return `${seconds} sec`
      }
      if (unit == 'min') {
        return `${Math.floor(seconds / 60)} min`
      }
      else if (unit == 'hr') {
        return `${Math.floor(seconds / (60 * 60))} hr`
      }
    }



    if (seconds < 60) {
      return `${Math.floor(seconds)} sec`
    }
    else if (seconds < 60 * 60) {

      return `${Math.floor(seconds / 60)} min`
    }
    else {
      return `${Math.floor(seconds / (60 * 60))} hr ${Math.floor((seconds % (60 * 60)) / 60)} min`
    }

  }
}



@Pipe({
  name: 'dateAgo',
  pure: true
})
export class DateAgoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      const seconds = Math.floor((+new Date() - +new Date(value)) / 1000);
      if (seconds < 29) // less than 30 seconds ago will show as 'Just now'
        return 'Just now';
      const intervals = {
        'year': 31536000,
        'month': 2592000,
        'week': 604800,
        'day': 86400,
        'hour': 3600,
        'minute': 60,
        'second': 1
      };
      let counter;
      for (const i in intervals) {
        counter = Math.floor(seconds / intervals[i]);
        if (counter > 0)
          if (counter === 1) {
            return counter + ' ' + i + ' ago'; // singular (1 day ago)
          } else {
            return counter + ' ' + i + 's ago'; // plural (2 days ago)
          }
      }
    }
    return value;
  }

}




@Pipe({
  name: 'orderBy',
  pure: true
})
export class OrderByPipe implements PipeTransform {

  transform(value: any[], properties: string[], order: 'asc' | 'dsc'): any[] {




    return value.sort((aa: any, bb: any) => {

      let a = aa
      let b = bb

      if (properties) {




        properties.forEach(p => {
          a = a[p]
          b = b[p]
        })
      }


      if (a.getTime) {
        return b.localeCompare(a)
      }

      if (a > b) {
        return (order && order == 'asc' ? 1 : -1)
      }
      else {
        return (order && order == 'asc' ? -1 : 1)
      }

    });




  }

}








@Pipe({ name: 'escape_katex' })
export class EscapeKatex implements PipeTransform {
  transform(value: string, args: string[]): any {
    // .replace(/(?<!\$)\$(?!\$)/g, '\\$')
    return value
  }

}





@Pipe({ name: 'preprocess_latex' })
export class PreprocessLatexPipe implements PipeTransform {
  transform(value: string, args: string[]): any {


    return value.replace(/\\subscript{(.*?)}{(.*?)}/g, '{$1}_{$2}')
      .replace(/\\superscript{(.*?)}{(.*?)}/g, '{$1}{$2}')
  }

}




@Pipe({ name: 'sum_field' })
export class SumPipe implements PipeTransform {
  transform(value: any[], fields?: string[]): number {
    let sum = 0;
    value.forEach(v => {

      if (!fields.length) {
        sum += v;

      }

      if (fields.length == 1) {
        sum += v[fields[0]] ? v[fields[0]] : 0
      }
      else if (fields.length == 2) {
        sum += v[fields[0]][fields[1]] ? v[fields[0]][fields[1]] : 0



      }


    })
    return sum;
  }
}


@Pipe({ name: 'avg_field' })
export class AvgPipe implements PipeTransform {
  transform(value: any[], fields?: string[]): number {
    let sum = 0;
    value.forEach(v => {

      if (!fields.length) {
        sum += v;

      }

      if (fields.length == 1) {
        sum += v[fields[0]] ? v[fields[0]] : 0
      }
      else if (fields.length == 2) {
        sum += v[fields[0]][fields[1]] ? v[fields[0]][fields[1]] : 0



      }




    })
    return sum / value.length;
  }
}
