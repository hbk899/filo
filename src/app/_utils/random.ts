import { firestore } from "firebase";

export const getCircularReplacer = () => {
    const seen = new WeakSet();
    return (key, value) => {
        if (typeof value === 'object' && value !== null) {
            if (seen.has(value)) {
                return;
            }
            seen.add(value);
        }
        return value;
    };
};

export const getReadableObject = (object) => {
    let a = '';
    Object.keys(object).forEach(key => {
        a += `${key}:${object[key]}
        `;
    });
    return a;
};




export const simple = (object) => {
    return JSON.parse(JSON.stringify(object))    
    
};

export const stampCreate = (object) => {
    object.createdAt = firestore.FieldValue.serverTimestamp();
    object.updatedAt = firestore.FieldValue.serverTimestamp();
    
    return object;
    
};


export const stampUpdate = (object) => {
    object.updatedAt = firestore.FieldValue.serverTimestamp();
    
    return object;
    
};