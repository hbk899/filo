import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule, ScreenTrackingService } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { ImageCropperModule } from 'ngx-image-cropper';

import { NgxDropzoneModule } from 'ngx-dropzone';

import * as firebase from 'firebase';

import { MarkdownModule } from 'ngx-markdown';

import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DemoMaterialModule } from './material.module';

import { AdminService } from './_services/admin.service';
import { AuthGuardService } from './_services/auth.gaurd';
import { AuthService } from './_services/auth.service';
import { DirectService } from './_services/direct.service';
import { NotifyService } from './_services/notify.service';
import { PostService } from './_services/post.service';
import { UploadService } from './_services/upload.service';
import { WindowService } from './_services/window.service';
import { KeysPipe, DeepCopyPipe, TrimPipe, KeysLengthPipe, SecondsToTimePipe, StringToDate, DateAgoPipe, OrderByPipe, EscapeKatex, PreprocessLatexPipe, SumPipe, AvgPipe } from './_utils/pipes';
import { AlertDialogComponent } from './components/alert-dialog/alert-dialog.component';
import { CardPostComponent } from './components/card-post/card-post.component';
import { ChipsSelectComponent } from './components/chips-select/chips-select.component';
import { EmailVerificationComponent } from './components/email-verification/email-verification.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TagsInputComponent } from './components/tags-input/tags-input.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { UploadComponent } from './components/upload/upload.component';
import { HomeScreen } from './screens/home/home.screen';
import { MyPostsScreen } from './screens/my-posts/my-posts.screen';
import { NewPostScreen } from './screens/new-post/new-post.screen';
import { PostDetailScreen } from './screens/post-detail/post-detail.screen';
import { SigninComponent } from './screens/signin/signin.component';
import { AdminArticleEditScreen } from './screens/admin/article-edit/article-edit.screen';
import { AdminArticlesScreen } from './screens/admin/articles/admin-articles.screen';
import { AdminUsersUserInfoDialog, AdminUsersScreen } from './screens/admin/users/admin-users.screen';
import { OrdersComponent } from './screens/orders/orders.component';
import { InventoryComponent } from './screens/inventory/inventory.component';
import { NewOrderComponent } from './screens/new-order/new-order.component';
import { ShipmentDetailComponent } from './screens/shipment-detail/shipment-detail.component' ;
// @dynamic
@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,

    NewPostScreen,
    MyPostsScreen,
    PostDetailScreen,
    AlertDialogComponent, AdminUsersUserInfoDialog,
    HomeScreen,
    AdminArticlesScreen,
    AdminArticleEditScreen,
    AdminUsersScreen,

    SidebarComponent,
    TopbarComponent,
    SigninComponent,
    UploadComponent,
    EmailVerificationComponent,
    TagsInputComponent,
    ChipsSelectComponent,



    CardPostComponent,

    KeysPipe,
    DeepCopyPipe,
    StringToDate,
    TrimPipe,
    DateAgoPipe,
    OrderByPipe,
    KeysLengthPipe,
    EscapeKatex,
    SecondsToTimePipe,
    SumPipe,
    AvgPipe,
    OrdersComponent,
    InventoryComponent,
    NewOrderComponent,
    ShipmentDetailComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DemoMaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,

    FormsModule, ReactiveFormsModule,

    // firebase
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireModule.initializeApp(environment.firebase), // imports firebase/app needed for everything
    AngularFirestoreModule.enablePersistence(), // imports firebase/firestore, only needed for database features
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AngularFireAnalyticsModule,

    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),


    ImageCropperModule,

    NgxDropzoneModule,

    MarkdownModule.forRoot(),


  ],
  providers: [



    AuthGuardService,
    AuthService,
    NotifyService,
    DirectService,
    WindowService,
    AdminService,
    UploadService,
    PostService

  ],
  bootstrap: [AppComponent],
  entryComponents: [AlertDialogComponent, AdminUsersUserInfoDialog]
})
export class AppModule { }
