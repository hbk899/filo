import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatChipInputEvent, MatAutocompleteSelectedEvent, MatTableDataSource, MatSort } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';

import { Observable, Subject, of } from 'rxjs';

import { firestore } from 'firebase';

import IUser from 'src/_schema/src/user';
import { PostService } from 'src/app/_services/post.service';

import { AuthService } from '../../_services/auth.service';
import { NotifyService } from '../../_services/notify.service';
import { Post } from './../../../_schema/src/post';






const TASKS = {
  CHOOSE_OPTION: 'choose_option',
  SUBMIT_LOADING: 'submit_loading',
  PRVIEW: 'submit_loading',

}
@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.screen.html',
  styleUrls: ['./post-detail.screen.scss']
})
export class PostDetailScreen implements OnInit {

  post: Post = {

    id: '',
    title: 'Title',
    description: 'The milk capacity of the cow is 18 litter, this cow is very good for a farm as well to have it in your Home as well',
    images: null,

    price: 56646,
    phoneNo: '923005674737',
    userID: '',


    keywords: ['cow', 'gaye'],
    category: ''
  };

  user: IUser;




  selectedImage = null

  contact_visible = false

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  loading: 'loading' | string;



  constructor(private authService: AuthService,
    private postService: PostService,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private notify: NotifyService,
    private location: Location,
  ) {


  }






  async ngOnInit(): Promise<void> {

    this.authService.mUser$.subscribe((user) => {
      if (user == null || user === undefined) {
        return;
      }

      this.user = user;

    });



    const postId = this.route.snapshot.paramMap.get('id');
    this.loading = 'loading';
    const post  = await this.postService.getPost(postId)
      .then(post => {
        this.post = post
        this.selectedImage = post.images[0]
        console.log(this.post);

        this.loading = null
      })
      .catch(err => {
        console.log(err);

        this.loading = null
      }) as any



  }



  chat_whatsapp() {
    window.open('https://wa.me/' + this.post.phoneNo, '_blank');


  }

  click_call() {

  }
  changeImgTo(image: any) {

    this.selectedImage = image;

  }

  showNumber() {

    this.contact_visible = true
  }





}



