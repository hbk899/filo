import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { MatIconRegistry } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import IUser from 'src/_schema/src/user';

import { AuthService } from '../../_services/auth.service';
import { NotifyService } from '../../_services/notify.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {


  task: 'signup' | 'signin' = 'signin';
  loading: true | false = false;
  forgotPassword = false


  bgUrl = 'http://backgroundcheckall.com/wp-content/uploads/2017/12/background-images-for-registration-page-10.jpg';
  email = '';
  password = '';
  confirmPassword = '';


  user = new IUser();




  loggedIn: boolean;


  newUser = false;


  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(6),

  ]);



  constructor(private authService: AuthService,
    private notifyService: NotifyService,
    private route: ActivatedRoute,
    private router: Router, public snackBar: MatSnackBar) { }

  ngOnInit() {




    this.forgotPassword = this.route.snapshot.paramMap.get('action') === 'forgot-password' ? true : false;

    this.authService.getLoggedInUpdates().subscribe(user => {
      console.log(user);

      if (!user) {

        if (this.route.snapshot.paramMap.get('action') === 'facebook') {

          this.authService.facebookLogin('redirect')

        }
        else if (this.route.snapshot.paramMap.get('action') === 'google') {

          this.authService.googleLogin('redirect')
        }
      }
      else {

        this.authService.syncUserToDb(user)
          .then(e => {
            this.notifyService.update('Logged in successfully', 'success')
            this.router.navigate(['/account']);
          })

      }
    })



  }


  _signInWithFacebook() {
    this.loading = true;
    this.authService.facebookLogin('popup')
      .then(result => {
        if (result) {
          return this.authService.syncUserToDb(result.user)
        }
        return null;
      })
      .then((user: IUser) => {
        console.log(user);
        if (user) {

          this.loading = false;
          this.router.navigate(['/account']);
        }

        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
        console.log(err);
      });

  }

  _signInWithGoogle() {
    this.loading = true;
    this.authService.googleLogin('popup')
      .then(result => {
        if (result) {
          return this.authService.syncUserToDb(result.user)
        }
        return null;
      })
      .then((user: IUser) => {
        console.log(user);
        if (user) {

          this.loading = false;
          this.router.navigate(['/account']);
        }

        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
        console.log(err);
      });

  }


  async _sendForgotPasswordEmail() {
    console.log(this.user.email);

    if (this.user.email) {
      this.loading = true
      await this.authService.sendResetPasswordEmail(this.user.email)
      this.forgotPassword = false
    }
    else {
      this.notifyService.update('Please enter email first', 'info')
    }
    this.loading = false
  }

  _submit() {
    this.loading = true;
    if (this.task === 'signin') {
      this.authService.emailLogin(this.user.email, this.password)
        .then(user => {

          this.loading = false;
          this.afterLogin(user as IUser);
        })
        .catch(error => {
          this.loading = false;


        })
    } else if (this.task === 'signup') {

      this.authService.emailSignUp(this.user, this.password)
        .then(user => {

          this.loading = false;
          this.afterLogin(user as IUser);
        })
        .catch(error => {
          this.loading = false;

        })



    }
  }


  afterLogin(user: IUser) {
    this.router.navigate(['/']);
  }

  toggleTask(task) {
    this.task = task as any;
  }



}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
