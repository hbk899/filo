import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatChipInputEvent, MatAutocompleteSelectedEvent, MatTableDataSource, MatSort } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';

import { Observable, Subject, of } from 'rxjs';

import { firestore } from 'firebase';

import IUser from 'src/_schema/src/user';
import { PostService } from 'src/app/_services/post.service';
import { Post } from 'src/_schema/src/post';

import { AuthService } from '../../_services/auth.service';
import { NotifyService } from '../../_services/notify.service';






const TASKS = {
  CHOOSE_OPTION: 'choose_option',
  SUBMIT_LOADING: 'submit_loading',
  PRVIEW: 'submit_loading',

}
@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.screen.html',
  styleUrls: ['./my-posts.screen.scss']
})
export class MyPostsScreen implements OnInit {




  user: IUser;



  @ViewChild(MatSort, { static: true }) sort: MatSort;
  posts: Post[];
  loading: any = true;





  constructor(private authService: AuthService,
    private postService: PostService,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private notify: NotifyService,
    private location: Location) {


  }





  ngOnInit(): void {

    this.authService.mUser$.subscribe((user) => {
      if (user == null || user === undefined) {
        return;
      }

      this.user = user;

    });


    this.postService.myPosts()
      .then(posts => {
        this.posts = posts;
        this.loading = null
      })

  }

}



