import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatChipInputEvent, MatAutocompleteSelectedEvent, MatTableDataSource, MatSort } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';
import { tick } from '@angular/core/testing';

import { Observable, Subject, of } from 'rxjs';

import { firestore } from 'firebase';

import IUser from 'src/_schema/src/user';
import { Post } from 'src/_schema/src/post';
import { Upload } from 'src/_schema/src/upload';
import { UploadService } from 'src/app/_services/upload.service';
import { Image } from 'src/_schema/src/image';
import { OFXFile } from 'src/_schema/src/storage';
import { PostService } from 'src/app/_services/post.service';

import * as uuid from 'uuid';

import { AuthService } from '../../_services/auth.service';
import { NotifyService } from '../../_services/notify.service';

const TASKS = {
  CHOOSE_OPTION: 'choose_option',
  SUBMIT_LOADING: 'submit_loading',
  PRVIEW: 'submit_loading',

}
@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.screen.html',
  styleUrls: ['./new-post.screen.scss']
})
export class NewPostScreen implements OnInit {


  user: IUser;

  post: Post;
  selectedCategory: string
  files: Upload[] = []
  postCategories = Post.ALL_CATEGORIES

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  loading: 'saving';
  error: string;



  constructor(private authService: AuthService,
    private router: Router,
    private uploadService: UploadService,
    private postService: PostService,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private notify: NotifyService,
    private location: Location) {


  }





  ngOnInit(): void {

    this.authService.mUser$.subscribe((user) => {
      if (user == null || user === undefined) {
        return;
      }
      this.user = user;
    });


    this.post = new Post(uuid())
    this.postCategories = Post.ALL_CATEGORIES

  }

  onSelect(event) {
    console.log(event);
    if (event.addedFiles[0] && event.addedFiles[0].name
      && (event.addedFiles[0].type === 'image/jpeg'
        || event.addedFiles[0].type === 'image/jpg'
        || event.addedFiles[0].type === 'image/png'
        || event.addedFiles[0].type === 'image/svg')) {

      const upload = new Upload(new OFXFile('/images/' + this.post.id, uuid()));
      upload.file = event.addedFiles[0]

      upload.loading = true;
      let fileInfo = null
      this.uploadService.uploadAndGetRef(upload)
        .then(ref => {
          fileInfo = ref
          console.log(ref);
          return ref.getDownloadURL();
        })
        .then(
          downloadUrl => {
            upload.loading = false;
            console.log(downloadUrl);
            upload.ofxFile.downloadURL = downloadUrl
            if (!this.post.images) {
              this.post.images = [upload.ofxFile]
            }
            else {
              this.post.images.push(upload.ofxFile)
            }
          }
        )
        .catch(err => {
          console.log(err);

        });


      this.files.push(upload);

    }
  }

  onRemove(upload: Upload) {
    console.log(upload);
    this.files.splice(this.files.indexOf(upload), 1);
    this.post.images.splice(this.files.indexOf(upload), 1);
    this.uploadService.deleteFile(upload.file)
  }


  async save() {
    this.loading = 'saving';
    this.error = null;
    await this.postService.savePost(this.post)
      .catch(e => {
        this.error = 'Some Error'
      })
    this.loading = null

    this.router.navigate(['/']);

  }
}



