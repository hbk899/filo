import { Component, OnInit, ViewChild, ElementRef, Renderer2, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatChipInputEvent, MatAutocompleteSelectedEvent } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';

import { Observable, Subject, of } from 'rxjs';

import { firestore } from 'firebase';
import IUser from 'src/_schema/src/user';

import { Tags } from 'src/_schema/src/tags';
import { Article } from 'src/_schema/src/article';
import { AdminService } from 'src/app/_services/admin.service';

import { UploadService } from 'src/app/_services/upload.service';

import { AuthService } from '../../../_services/auth.service';
import { NotifyService } from '../../../_services/notify.service';
import { Upload } from 'src/_schema/src/upload';



const TASKS = {
  CHOOSE_OPTION: 'choose_option',
  SUBMIT_LOADING: 'submit_loading',
  PRVIEW: 'submit_loading',

}


@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.screen.html',
  styleUrls: ['./article-edit.screen.scss']
})
export class AdminArticleEditScreen implements OnInit {

  task: 'new-article' | 'update-article' = 'new-article';
  loading: 'saving-article' | 'none' = null;


  user: IUser;

  articles: Article[];

  article: Article = new Article


  @ViewChild('editor', { static: true }) editor: ElementRef;

  textProcessors = {
    make_summary: (txt: string) => {
      return `<details>
      <summary>Show</summary>
      <p>${txt}</p>
</details>`
    },


    link: (url: string) => {
      return `[Link](${url})`
    },


    italic: (txt: string) => {
      return `*${txt}*`
    },


    bold: (txt: string) => {
      return `**${txt}**`
    },



    sup: (txt: string) => {
      return `<sup>${txt}</sup>`
    },

    sub: (txt: string) => {
      return `<sub>${txt}</sub>`
    },


    math: (txt: string) => {
      return `$$ ${txt} $$`
    },


    list: (txt: string) => {
      return `\n* ${txt}`
    },
  }



  constructor(private authService: AuthService,
    private adminService: AdminService,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private rd: Renderer2,
    private router: Router,
    private _formBuilder: FormBuilder,
    private notify: NotifyService,
    private location: Location) {


  }





  ngOnInit(): void {

    this.authService.getLoggedInUpdates().subscribe((user) => {




      if (user == null || user === undefined || (user && !(user.email === 'bilalkhalid2240@gmail.com' || user.email === 'jan.arifullah@gmail.com'))) {
        this.notify.update('You are not authorized, please signin with correct account', 'success')
        this.router.navigate(['/signin'])
      }


      const mainID = this.route.snapshot.paramMap.get('id');

      if (mainID) {

        firestore().collection('articles').doc(mainID).get()
          .then(doc => {
            this.task = 'update-article'
            this.loading = null;

            this.article = doc.data() as Article;
            this._retrieveArticleFiles();
          })
          .catch(e => {

          })

      }
      else {
        this.article = new Article

        this.article.id = this.adminService.getNewID()
        this.article.body = 'write something...'
        this.task = 'new-article'
        this.loading = null
      }


      document.addEventListener('keydown', (e) => {
        if (e.key === 's' && e.ctrlKey === true) {
          e.preventDefault(); // to prevent default ctrl+s action

          this._save();
        }
      });


    });




  }



  _retrieveArticleFiles() {
    this.adminService.getFilesForArticle(this.article.id)
      .then(d => {
        console.log('article files', d);

      })
      .catch(e => console.log(e));
  }


  _onTopicsChanged(topics: string[]) {
    this.article.topics = Tags.convertToTags(topics)
  }



  _onLOSChanged(los: string[]) {
    this.article.los = los.join(',')
  }

  async _save() {

    console.log(this.article);

    this.loading = 'saving-article'
    if (this.task === 'update-article') {
      await this.adminService.updateArticle(this.article)
    }
    else {
      this.article = await this.adminService.newArticle(this.article)
      this.task = 'update-article'
    }

    this.loading = null


  }

  async _onFileDroped(e) {
    console.log(e);
    if (e.addedFiles[0] && e.addedFiles[0].name
      && (e.addedFiles[0].type === 'image/jpeg'
        || e.addedFiles[0].type === 'image/jpg'
        || e.addedFiles[0].type === 'image/png'
        || e.addedFiles[0].type === 'image/svg')) {


      const upload = new Upload({
        path: '/article-images/' + this.article.id + '/' + Math.random()
      } as any);
      upload.file = e.addedFiles[0]


      upload.loading = true;
      let fileInfo = null
      this.uploadService.uploadAndGetRef(upload)
        .then(ref => {
          fileInfo = ref
          console.log(ref);

          return ref.getDownloadURL();
        })
        .then(
          downloadUrl => {
            upload.loading = false;

            this.insertToBody(
              `\n[//]: # (${JSON.stringify({ ...fileInfo.location })})\n`
              + this.generateImageTag(downloadUrl)
              + `\n`)


          }
        )
        .catch(err => {
          console.log(err);

        });
    }

  }
  onKeyUp($event): void {


    const charCode = String.fromCharCode($event.which).toLowerCase();

    if ($event.ctrlKey && charCode === 'c') {

    } else if ($event.ctrlKey && charCode === 'v') {

    } else if ($event.ctrlKey && charCode === 's') {
      $event.preventDefault();

      this._save();
    }
  }

  generateImageTag(url: string): string {
    return `![------Add Image Title ----------](${url})`
  }


  insertToBody(textToInsert) {
    console.log(this.editor);

    const cursorP = this.editor.nativeElement.selectionStart;
    this.article.body = this.article.body.slice(0, cursorP) + textToInsert + this.article.body.slice(cursorP);
  }


  processSelection(processor: (str: string) => string) {
    console.log((this.editor.nativeElement.value)
      .substring(this.editor.nativeElement.selectionStart, this.editor.nativeElement.selectionEnd));

    const start = this.editor.nativeElement.selectionStart
    const end = this.editor.nativeElement.selectionEnd
    const result = this.article.body.slice(0, start)
      + processor(this.article.body.slice(start, end))
      + this.article.body.slice(end)

    document.execCommand('insertText', false, processor(this.article.body.slice(start, end)));


  }


}



