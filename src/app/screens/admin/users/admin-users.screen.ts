import { Component, OnInit, ViewChild, ElementRef, SimpleChanges, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatChipInputEvent, MatAutocompleteSelectedEvent, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';

import { Observable, Subject, of } from 'rxjs';

import { firestore } from 'firebase';

import IUser from 'src/_schema/src/user';
import { AdminService } from 'src/app/_services/admin.service';


import { AuthService } from '../../../_services/auth.service';
import { NotifyService } from '../../../_services/notify.service';



const TASKS = {
  CHOOSE_OPTION: 'choose_option',
  SUBMIT_LOADING: 'submit_loading',
  PRVIEW: 'submit_loading',

}
@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.screen.html',
  styleUrls: ['./admin-users.screen.scss']
})
export class AdminUsersScreen implements OnInit {




  user: IUser;



  displayedColumns: string[] = ['id', 'displayName','email', 'programs', 'createdAt', 'updatedAt', 'options'];
  dataSource = new MatTableDataSource([]);
  articleList: string;



  constructor(private authService: AuthService,
    private adminService: AdminService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private router: Router,
    private _formBuilder: FormBuilder,
    private notify: NotifyService,
    private location: Location) {


  }




  @ViewChild(MatSort, { static: true }) sort: MatSort;


  ngOnInit(): void {

    this.authService.getLoggedInUpdates().subscribe((user) => {




      if (user == null || user === undefined || (user && !(user.email === 'bilalkhalid2240@gmail.com' || user.email === 'jan.arifullah@gmail.com'))) {
        this.notify.update('You are not authorized, please signin with correct account', 'success')
        this.router.navigate(['/signin'])
      }


    });







    firestore().collection('users')
      .get()
      .then(collection => {



        this.dataSource = new MatTableDataSource(collection.docs.map(doc => doc.data()));
        this.dataSource.sort = this.sort;

        this.dataSource.sortingDataAccessor = (data, header) => data[header];
      })
      .catch(e => {

      })


  }

  _onChange() {




  }

  _applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  _openInfo(user: IUser): void {
    const dialogRef = this.dialog.open(AdminUsersUserInfoDialog, {
      width: '80%',
      data: JSON.parse(JSON.stringify(user))
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }



}





@Component({
  selector: 'admin-users-info-dialog',
  templateUrl: 'admin-users-info-dialog.html',
})
export class AdminUsersUserInfoDialog {

  constructor(
    public dialogRef: MatDialogRef<AdminUsersUserInfoDialog>,
    @Inject(MAT_DIALOG_DATA) public data: IUser) { }

  _close(): void {
    this.dialogRef.close();
  }

}
