import { Component, OnInit, ViewChild, ElementRef, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatChipInputEvent, MatAutocompleteSelectedEvent, MatSort, MatTableDataSource } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';

import { Observable, Subject, of } from 'rxjs';

import { firestore } from 'firebase';

import { AdminService } from 'src/app/_services/admin.service';

import { AuthService } from '../../../_services/auth.service';
import { NotifyService } from '../../../_services/notify.service';
import IUser from 'src/_schema/src/user';
import { Article } from 'src/_schema/src/article';



const TASKS = {
  CHOOSE_OPTION: 'choose_option',
  SUBMIT_LOADING: 'submit_loading',
  PRVIEW: 'submit_loading',

}
@Component({
  selector: 'app-admin-articles',
  templateUrl: './admin-articles.screen.html',
  styleUrls: ['./admin-articles.screen.scss']
})
export class AdminArticlesScreen implements OnInit {

  task: "new-article" | "update-article" = "new-article";
  loading: "saving-article" | "none" = null;


  user: IUser;



  allTopics = []


  articles: Article[];





  displayedColumns: string[] = ['url', 'title', 'los', 'createdAt', 'updatedAt', 'options'];
  dataSource = new MatTableDataSource([]);
  articleList: string;



  constructor(private authService: AuthService,
    private adminService: AdminService,
    private route: ActivatedRoute,

    private router: Router,
    private _formBuilder: FormBuilder,
    private notify: NotifyService,
    private location: Location) {


  }




  @ViewChild(MatSort, { static: true }) sort: MatSort;


  ngOnInit(): void {

    this.authService.getLoggedInUpdates().subscribe((user) => {




      if (user == null || user === undefined || (user && !(user.email === 'bilalkhalid2240@gmail.com' || user.email === 'jan.arifullah@gmail.com'))) {
        this.notify.update('You are not authorized, please signin with correct account', 'success')
        this.router.navigate(['/signin'])
      }


    });



    // firestore().collection('articles').orderBy('createdAt').limit(30)
    //   .onSnapshot(docs => {
    // const articles: Article[] = [];
    // docs.docs.forEach(doc => {
    //   articles.push(doc.data() as Article);
    // })
    // this.articles = articles;


    // this.dataSource = new MatTableDataSource(this.articles);


    // this.dataSource.sort = this.sort;

    //   })



    firestore().collection('manual').doc('article-list')
      .get()
      .then(articleListDoc => {


        this.dataSource = new MatTableDataSource(Object.values(articleListDoc.data()));
        this.dataSource.sort = this.sort;

        this.dataSource.sortingDataAccessor = (data, header) => data[header];
      })
      .catch(e => {

      })


  }

  _onChange() {




  }

  _applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }





}


