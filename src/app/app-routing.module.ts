import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from './_services/auth.gaurd';
import { HomeScreen } from './screens/home/home.screen';
import { MyPostsScreen } from './screens/my-posts/my-posts.screen';
import { NewPostScreen } from './screens/new-post/new-post.screen';
import { PostDetailScreen } from './screens/post-detail/post-detail.screen';
import { SigninComponent } from './screens/signin/signin.component';
import { AdminArticleEditScreen } from './screens/admin/article-edit/article-edit.screen';
import { AdminArticlesScreen } from './screens/admin/articles/admin-articles.screen';
import { AdminUsersScreen } from './screens/admin/users/admin-users.screen';
import { OrdersComponent } from './screens/orders/orders.component';
import { NewOrderComponent } from './screens/new-order/new-order.component';
import { InventoryComponent } from './screens/inventory/inventory.component';


const routes: Routes = [
  { path: '', redirectTo: 'find', pathMatch: 'full' },
  {
    path: 'find',
    component: HomeScreen
  },
  {
    path: 'new-post',
    component: NewPostScreen
   
  },
  {
    path: 'my-posts',
    component: MyPostsScreen,
    canActivate: [AuthGuardService]
  },

  {
    path: 'post/:id',
    component: PostDetailScreen,
    canActivate: [AuthGuardService]
  },
  {
    path: 'signin',
    component: SigninComponent
  },

  {
    path: 'admin-articles',
    component: AdminArticlesScreen,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin-users',
    component: AdminUsersScreen,
    canActivate: [AuthGuardService]
  },

  {
    path: 'admin-new-article',
    component: AdminArticleEditScreen,
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin-article/:id',
    component: AdminArticleEditScreen,
    canActivate: [AuthGuardService]
  },

  {

    path : 'app-orders',
    component : OrdersComponent
    
  }
  ,

  {
    path : 'app-new-order' ,
    component : NewOrderComponent
  

  }
,

  {
    path : 'app-inventory',
    component : InventoryComponent

  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
