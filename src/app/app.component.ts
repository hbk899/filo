import { Component, OnInit, HostListener } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';

import IUser from 'src/_schema/src/user';


import { AuthService } from './_services/auth.service';

import { WindowService } from './_services/window.service';


// import{ MatSidenav} from '@angular/material/sidenav';
// import{ MatSidenavContent} from '@angular/material/sidenav';
// import{ MatSidenavContainer} from '@angular/material/sidenav';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'BiluMandi.com';




  user = new IUser;


  username: any = '';
  userId: any;
  profileUrl: any;
  isDoctor = false;
  loggedIn: any = true;
  notloggedIn: any = true;
  sideNavOpen  = true;
  sideNavOpened  = true;
  sideNavMode = 'push';


  isMobile = false;


  constructor(private router: Router,
    public snackBar: MatSnackBar,
    public windowService: WindowService,
    public dialog: MatDialog,
    private authService: AuthService) { }



  ngOnInit() {
    this.router.events.subscribe((evt) => {



      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
    this.resubscribe();

    this.authService.getLoggedInUpdates().subscribe((authUser) => {
      this.resubscribe();
      if (authUser != null) {
        this.loggedIn = true;
        this.notloggedIn = false;
      } else {

        this.loggedIn = false;
        this.notloggedIn = true;
      }

    });


    this.refresh();

  }


  resubscribe() {

    console.log('resubscribing');

    this.authService.mUser$.subscribe((user) => {
      if (!user) {
        return;
      }

      this.user = user;

    });
  }

  logout() {
    localStorage.clear();
    this.userId = 0;
    this.loggedIn = false;
    this.notloggedIn = true;
    this.username = '';
  }


  refresh() {
    const w = window,
      d = document,
      e = d.documentElement,
      g = d.getElementsByTagName('body')[0],
      x = w.innerWidth || e.clientWidth || g.clientWidth,
      y = w.innerHeight || e.clientHeight || g.clientHeight;


    console.log(x);

    if (x > 600) {
      this.isMobile = false;
      this.sideNavOpened = false;
      this.sideNavMode = 'side';
    } else {
      this.sideNavMode = 'over';
      this.sideNavOpened = false;
      this.isMobile = true;
    }
  }




  onSectionChange(sectionId: string) {
    console.log(sectionId);
    this.windowService.currentSection = sectionId;
  }

}

